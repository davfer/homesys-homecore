package main

import (
	"context"
	"flag"
	"fmt"
	"gitlab.com/davfer/homesys-homecore/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/testdata"
	"log"
	"net"
)

var (
	tls        = flag.Bool("tls", false, "Connection uses TLS if true, else plain TCP")
	certFile   = flag.String("cert_file", "", "The TLS cert file")
	keyFile    = flag.String("key_file", "", "The TLS key file")
	jsonDBFile = flag.String("json_db_file", "", "A json file containing a list of features")
	port       = flag.Int("port", 4380, "The server port")
)

type homecoreServer struct {
}

func (h *homecoreServer) AuthenticateMyself(ctx context.Context, cred *api.Credentials) (*api.Permission, error) {
	return &api.Permission{
		Conceded: true,
		Key:      "ThisIsAPassKey",
	}, nil
}

func (h *homecoreServer) GetSystem(ctx context.Context, req *api.SystemRequest) (*api.SystemDescriptor, error) {
	endpoint := ""
	if req.Name == "NODE_SVC" {
		endpoint = "localhost:4385"
	} else if req.Name == "STORE_SVC" {
		endpoint = "localhost:4383"
	} else if req.Name == "DATABASE_SVC" {
		endpoint = "localhost:4386"
	}

	return &api.SystemDescriptor{
		Endpoint: endpoint,
	}, nil
}

func main() {
	server := &homecoreServer{}

	flag.Parse()
	lis, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	var opts []grpc.ServerOption
	if *tls {
		if *certFile == "" {
			*certFile = testdata.Path("server1.pem")
		}
		if *keyFile == "" {
			*keyFile = testdata.Path("server1.key")
		}
		creds, err := credentials.NewServerTLSFromFile(*certFile, *keyFile)
		if err != nil {
			log.Fatalf("Failed to generate credentials %v", err)
		}
		opts = []grpc.ServerOption{grpc.Creds(creds)}
	}

	grpcServer := grpc.NewServer(opts...)
	api.RegisterServiceAuthServer(grpcServer, server)
	grpcServer.Serve(lis)
}
